import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();


describe("Users happy path", () => {
    let accessToken: string;
    let userid: number;

    let useremail: string;
    let userpassword: string;
    let username: string;

    useremail = "mycardog@gmail.com";
    username = "myhorse";
    userpassword = "myhorse";

    //1 ) New User Registration: POST /api/Register
    it(`Should create a new user and validate status code`, async () => {
        let response = await auth.userregister(useremail, username, userpassword);
        
        accessToken = response.body.token.accessToken.token;
        userid = response.body.user.id;

        //Test status code is 201
        checkStatusCode(response, 201);

    });

    // 2) Get all Users information : GET /api/Users
    it(`Should return the list of all Users and check the body is not empty`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
          
    });    


    // 3. Login using creds from the 1st step POST /api/Auth/login
    it(`Should login with creds from step 1 and check body contains userName field`, async () => {
        let response = await auth.login(useremail, userpassword);
 
        accessToken = response.body.token.accessToken.token;
        checkStatusCode(response, 200);
        expect(response.body.user, `Response body should contain userName`).to.have.property("userName");
    });


    //4. Get details of current users, who logged in: GET /api/Users/fromToken
    it(`Should return details of current user who logged in`, async () => {
        let response = await users.getCurrentUser(accessToken);
        
        checkStatusCode(response, 200);
        expect(response.body, `Response body should contain email`).to.have.property("email");
    });


    //5 Update current user
    it(`Should update current user, set new avatar image and check return code`, async () => {
        let userData: object = {
            id: userid,
            avatar: "https://cdnn21.img.ria.ru/images/07e5/06/18/1738448523_0:54:864:540_1920x0_80_0_0_22bd72aa578b3fece6a89a620c95c4a1.jpg",
            email: useremail,
            userName: username,
        };

        let response = await users.updateUser(userData, accessToken);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);

        //Check if body is not empty 
        expect(response.body).to.be.empty;
    });


    //6 Get details of currecnt user who logged in: GET /api/Users/fromToken
    it(`Should return summary after updating of current user who logged in and check new avatar value`, async () => {
        let response = await users.getCurrentUser(accessToken);
    
        checkStatusCode(response, 200);
        expect(response.body, `Avatar should contain an image path`).to.have.property("avatar").that.equals("https://cdnn21.img.ria.ru/images/07e5/06/18/1738448523_0:54:864:540_1920x0_80_0_0_22bd72aa578b3fece6a89a620c95c4a1.jpg");
    });


    //7 Get details of current Users by id: GET /api/Users/{id}

    it(`should return user details by id`, async () => {
            let response = await users.getUserById(userid);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    //8 Delete current User by ID: DELETE /api/Users/{id}

    it(`should delete user by id`, async () => {
        let response = await users.deleteUser(userid,accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });



});
