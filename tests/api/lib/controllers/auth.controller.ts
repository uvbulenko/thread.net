import { ApiRequest } from "../request";

export class AuthController {

    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }


    async userregister(emailValue: string, usernameValue:string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                id: 0,
                email: emailValue,
                userName: usernameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }






}
